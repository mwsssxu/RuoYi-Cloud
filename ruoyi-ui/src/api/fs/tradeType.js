import request from '@/utils/request'

// 状态修改
export function changeTradetypeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/financial/tradeType/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询交易类型列表
export function listTradeType(query) {
  return request({
    url: '/financial/tradeType/list',
    method: 'get',
    params: query
  })
}

// 查询交易类型详细
export function getTradeType(id) {
  return request({
    url: '/financial/tradeType/' + id,
    method: 'get'
  })
}

// 新增交易类型
export function addTradeType(data) {
  return request({
    url: '/financial/tradeType',
    method: 'post',
    data: data
  })
}

// 修改交易类型
export function updateTradeType(data) {
  return request({
    url: '/financial/tradeType',
    method: 'put',
    data: data
  })
}

// 删除交易类型
export function delTradeType(id) {
  return request({
    url: '/financial/tradeType/' + id,
    method: 'delete'
  })
}

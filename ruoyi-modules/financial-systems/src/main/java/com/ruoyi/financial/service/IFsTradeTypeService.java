package com.ruoyi.financial.service;

import com.ruoyi.financial.domain.FsTradeType;
import com.ruoyi.system.api.domain.SysUser;

import java.util.List;

/**
 * 交易类型Service接口
 * 
 * @author ruoyi
 * @date 2022-12-29
 */
public interface IFsTradeTypeService 
{
    /**
     * 查询交易类型
     * 
     * @param id 交易类型主键
     * @return 交易类型
     */
    public FsTradeType selectFsTradeTypeById(Long id);

    /**
     * 查询交易类型列表
     * 
     * @param fsTradeType 交易类型
     * @return 交易类型集合
     */
    public List<FsTradeType> selectFsTradeTypeList(FsTradeType fsTradeType);

    /**
     * 新增交易类型
     * 
     * @param fsTradeType 交易类型
     * @return 结果
     */
    public int insertFsTradeType(FsTradeType fsTradeType);

    /**
     * 修改交易类型
     * 
     * @param fsTradeType 交易类型
     * @return 结果
     */
    public int updateFsTradeType(FsTradeType fsTradeType);

    /**
     * 批量删除交易类型
     * 
     * @param ids 需要删除的交易类型主键集合
     * @return 结果
     */
    public int deleteFsTradeTypeByIds(Long[] ids);

    /**
     * 删除交易类型信息
     * 
     * @param id 交易类型主键
     * @return 结果
     */
    public int deleteFsTradeTypeById(Long id);

    int updateStatus(FsTradeType fsTradeType);
}

package com.ruoyi.financial.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.financial.domain.FsTradeType;
import com.ruoyi.financial.service.IFsTradeTypeService;
import com.ruoyi.system.api.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 交易类型Controller
 * 
 * @author ruoyi
 * @date 2022-12-29
 */
@RestController
@RequestMapping("/tradeType")
public class FsTradeTypeController extends BaseController
{
    @Autowired
    private IFsTradeTypeService fsTradeTypeService;

    /**
     * 查询交易类型列表
     */
    @GetMapping("/list")
    public TableDataInfo list(FsTradeType fsTradeType)
    {
        startPage();
        List<FsTradeType> list = fsTradeTypeService.selectFsTradeTypeList(fsTradeType);
        return getDataTable(list);
    }

    /**
     * 导出交易类型列表
     */
    @Log(title = "交易类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FsTradeType fsTradeType)
    {
        List<FsTradeType> list = fsTradeTypeService.selectFsTradeTypeList(fsTradeType);
        ExcelUtil<FsTradeType> util = new ExcelUtil<FsTradeType>(FsTradeType.class);
        util.exportExcel(response, list, "交易类型数据");
    }

    /**
     * 获取交易类型详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(fsTradeTypeService.selectFsTradeTypeById(id));
    }

    /**
     * 新增交易类型
     */
    @Log(title = "交易类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody FsTradeType fsTradeType)
    {
        return toAjax(fsTradeTypeService.insertFsTradeType(fsTradeType));
    }

    /**
     * 修改交易类型
     */
    @Log(title = "交易类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FsTradeType fsTradeType)
    {
        return toAjax(fsTradeTypeService.updateFsTradeType(fsTradeType));
    }

    /**
     * 删除交易类型
     */
    @Log(title = "交易类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fsTradeTypeService.deleteFsTradeTypeByIds(ids));
    }

    /**
     * 状态修改
     */
    @Log(title = "交易类型状态变更", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody FsTradeType fsTradeType)
    {
        fsTradeType.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(fsTradeTypeService.updateStatus(fsTradeType));
    }

}

package com.ruoyi.financial.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.financial.domain.FsTradeType;
import com.ruoyi.financial.mapper.FsTradeTypeMapper;
import com.ruoyi.financial.service.IFsTradeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 交易类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-12-29
 */
@Service
public class FsTradeTypeServiceImpl implements IFsTradeTypeService
{
    @Autowired
    private FsTradeTypeMapper fsTradeTypeMapper;

    /**
     * 查询交易类型
     * 
     * @param id 交易类型主键
     * @return 交易类型
     */
    @Override
    public FsTradeType selectFsTradeTypeById(Long id)
    {
        return fsTradeTypeMapper.selectFsTradeTypeById(id);
    }

    /**
     * 查询交易类型列表
     * 
     * @param fsTradeType 交易类型
     * @return 交易类型
     */
    @Override
    public List<FsTradeType> selectFsTradeTypeList(FsTradeType fsTradeType)
    {
        return fsTradeTypeMapper.selectFsTradeTypeList(fsTradeType);
    }

    /**
     * 新增交易类型
     * 
     * @param fsTradeType 交易类型
     * @return 结果
     */
    @Override
    public int insertFsTradeType(FsTradeType fsTradeType)
    {
        fsTradeType.setCreateTime(DateUtils.getNowDate());
        return fsTradeTypeMapper.insertFsTradeType(fsTradeType);
    }

    /**
     * 修改交易类型
     * 
     * @param fsTradeType 交易类型
     * @return 结果
     */
    @Override
    public int updateFsTradeType(FsTradeType fsTradeType)
    {
        fsTradeType.setUpdateTime(DateUtils.getNowDate());
        return fsTradeTypeMapper.updateFsTradeType(fsTradeType);
    }

    /**
     * 批量删除交易类型
     * 
     * @param ids 需要删除的交易类型主键
     * @return 结果
     */
    @Override
    public int deleteFsTradeTypeByIds(Long[] ids)
    {
        return fsTradeTypeMapper.deleteFsTradeTypeByIds(ids);
    }

    /**
     * 删除交易类型信息
     * 
     * @param id 交易类型主键
     * @return 结果
     */
    @Override
    public int deleteFsTradeTypeById(Long id)
    {
        return fsTradeTypeMapper.deleteFsTradeTypeById(id);
    }

    /**
     * 修改交易类型状态
     *
     * @param fsTradeType 交易类型信息
     * @return 结果
     */
    @Override
    public int updateStatus(FsTradeType fsTradeType)
    {
        return fsTradeTypeMapper.updateFsTradeType(fsTradeType);
    }

}
